package co.getbarry;

import co.getbarry.util.BarryUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.lable.oss.uniqueid.GeneratorException;
import org.lable.oss.uniqueid.IDGenerator;
import org.lable.oss.uniqueid.LocalUniqueIDGeneratorFactory;
import org.lable.oss.uniqueid.bytes.Mode;

import static org.assertj.core.api.Assertions.assertThat;

public class TestIdGen {

    @Test
    public void test() throws GeneratorException {
        final int generatorID = 0;
        final int clusterID = 0;
        IDGenerator generator = LocalUniqueIDGeneratorFactory.generatorFor(generatorID, clusterID, Mode.SPREAD);
// Generate IDs
        for (int i=0; i < 1000; i ++) {
            byte[] idBytes = generator.generate();
            String id = BarryUtil.encodeHex(idBytes);
            assertThat(id).endsWith("0000");
            System.out.println(StringUtils.removeEnd(id, "0000"));
        }
    }
}
