package co.getbarry.api.slimpay;

import co.getbarry.api.slimpay.model.*;
import co.getbarry.util.exception.EntityNotFoundException;
import co.getbarry.util.http.HttpAuthRequestBuilder;
import co.getbarry.util.iso.MoneyToolbox;
import co.getbarry.util.network.NetworkToolbox;
import com.fasterxml.jackson.databind.JsonNode;
import org.eastinghouse.model.simplex.type.Country;
import org.eastinghouse.model.simplex.type.Currency;
import org.junit.jupiter.api.Test;

import javax.money.MonetaryAmount;
import java.time.ZonedDateTime;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class SlimpayRawServiceTest {

    public static final String slimpayServerurl = "https://api.preprod.slimpay.com";
    public static final String slimpayApiId = "barryfrancesas01";
    public static final String slimpayApiSecret = "rrTIdqLLBFHf8IhBhWzNPTmQExq3YYuY0F0IxjRT";
    public static final String slimpayCreditorReference = "barryfrancesas";

    public static SlimpayRawService newSlimpayRawService() {
        return new SlimpayRawService(slimpayServerurl, slimpayApiId, slimpayApiSecret);
    }

    private SlimpayRawService slimpayRawService = newSlimpayRawService();

    // Test Entry Point : https://api.preprod.slimpay.com/
    // Test Token endpoint : https://api.preprod.slimpay.net/oauth/token
    // - Creditor_reference: barryfrancesas
    // - App_name: barryfrancesas01
    @Test
    public void oAuthAndGetServiceLinks_shouldBeOk() {
        JsonNode response = slimpayRawService.getOAuth2Client().executeJson(
                HttpAuthRequestBuilder.newGet(NetworkToolbox.newUriValid("https://api.preprod.slimpay.com"), "").build(),
                JsonNode.class
        );
        //response.get("_links").get("https://api.slimpay.net/alps#create-orders").get("href")
        assertThat(response.get("_links")).isNotNull().isNotEmpty();
    }

    @Test
    public void importAndPayWithMandate_shouldBeOk() throws EntityNotFoundException {
        Contact contact = Contact.builder()
                .referenceId("manualXav")
                .honorificPrefix("Mr")
                .givenName("Xav")
                .familyName("Moi")
                .email("xpallot@etiskapp.com")
                .telephone("+33607447797")
                .billingAddress(Address.builder().street("rue du du").city("Paris").postalCode("75000").country(Country.FR).build())
                .bankAccount(BankAccount.builder().iban("FR7630001007941234567890185").build())
                .build();

        Mandate mandate = slimpayRawService.importMandate(slimpayCreditorReference, contact, ZonedDateTime.now(), null);
        assertThat(mandate.getReference()).isNotBlank();

        Mandate mandateFull = slimpayRawService.findMandate(slimpayCreditorReference, mandate.getReference());
        assertThat(mandateFull.getReference()).isNotBlank().isEqualTo(mandate.getReference());

        MonetaryAmount amount = MoneyToolbox.newEurMoneyOf(166);
        Payment payment = slimpayRawService.payByDirectDebitWithMandate(slimpayCreditorReference, mandate.getReference(), amount);

        assertThat(payment.getCurrency()).isEqualTo(Currency.EUR.name());
        assertThat(MoneyToolbox.newEurMoneyOf(payment.getAmount())).isEqualTo(amount);
    }

}
