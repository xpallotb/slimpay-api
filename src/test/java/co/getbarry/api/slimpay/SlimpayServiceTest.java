package co.getbarry.api.slimpay;


import co.getbarry.model.db.payment_mean.SepaDirectDebitMandate;
import co.getbarry.model.db.payment_mean.testing.SepaDirectDebitMandateRandom;
import co.getbarry.util.exception.EntityNotFoundException;
import co.getbarry.util.iso.MoneyToolbox;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class SlimpayServiceTest {

    private SlimpayService slimpayService = new SlimpayService(SlimpayRawServiceTest.newSlimpayRawService());

    @Test
    public void signImportAndPayMandate_OK() throws EntityNotFoundException {
        SepaDirectDebitMandate sepaDirectDebitMandate = new SepaDirectDebitMandateRandom(Locale.FRANCE)
                .with(m -> m.debtorIban("FR7630001007941234567890185"))
                .barryCreditor()
                .signatureFake()
                .build();

        sepaDirectDebitMandate = slimpayService.importMandate(sepaDirectDebitMandate);

        String paymentReference = slimpayService.payByDirectDebit(sepaDirectDebitMandate, MoneyToolbox.newEurMoneyOf(555));
        assertThat(paymentReference).isNotBlank();
    }

    @Test
    public void importUnsignedMandate_Failed() throws EntityNotFoundException {
        SepaDirectDebitMandate sepaDirectDebitMandate = new SepaDirectDebitMandateRandom(Locale.FRANCE)
                .with(m -> m.debtorIban("FR7630001007941234567890185"))
                .barryCreditor().build();

        assertThatThrownBy(() -> slimpayService.importMandate(sepaDirectDebitMandate))
                .isInstanceOf(IllegalArgumentException.class);
    }


}
