package co.getbarry.util.http;

import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth.OAuth20Service;

public class OAuth2Client extends HttpAuthClient<OAuth20Service, OAuth2AccessToken> {

    public OAuth2Client(OAuth20Service oAuth20Service, AccessTokenSupplier<OAuth2AccessToken> accessTokenSupplier) {
        super(oAuth20Service, accessTokenSupplier);
    }

    @Override
    protected OAuthRequest signRequest(OAuth20Service oAuthService, OAuthRequest oAuthRequest, OAuth2AccessToken accessToken) {
        oAuthService.signRequest(accessToken, oAuthRequest);
        return oAuthRequest;
    }

}
