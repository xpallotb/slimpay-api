package co.getbarry.util.http;

import co.getbarry.util.Json;
import co.getbarry.util.UnexpectedTechnicalException;
import co.getbarry.util.exception.EntityNotFoundException;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Token;
import com.github.scribejava.core.oauth.OAuthService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class HttpAuthClient<TOAuthService extends OAuthService, TToken extends Token> {

    private static final Logger LOG = LoggerFactory.getLogger(HttpAuthClient.class);
    private TOAuthService oAuthService;
    private AccessTokenSupplier<TToken> accessTokenSupplier;
    private TToken accessToken;

    public interface AccessTokenSupplier<TToken extends Token> {
        TToken getAccessToken() throws Exception;
    }

    public HttpAuthClient(TOAuthService oAuthService, AccessTokenSupplier<TToken> accessTokenSupplier) {
        this.oAuthService = oAuthService;
        this.accessTokenSupplier = accessTokenSupplier;
    }

    public <TEntity> TEntity executeJsonAndCheckNotFound(OAuthRequest oAuthRequest, Class<TEntity> entityClass) throws EntityNotFoundException {
        try {
            return executeJson(oAuthRequest, entityClass);
        } catch (HttpResponseException e) {
            throw e.checkEntityNotFoundException(entityClass);
        }
    }

    public <TEntity> TEntity executeJson(OAuthRequest oAuthRequest, Class<TEntity> entityClass) {
        // oAuthRequest.addHeader("Accept", MediaType.JSON_UTF_8.toString());
        Response response = executeRequest(oAuthRequest);
        return readJsonBody(response, entityClass);
    }

    public <TEntity> TEntity readJsonBody(Response response, Class<TEntity> entityClass) {
        try {
            String body = response.getBody();
            // handle case of empty body (http code 204 NO_CONTENT)
            if (StringUtils.length(body) <= 0) {
                return null;
            }
            TEntity entity = Json.parse(body, entityClass);
            return entity;
        } catch (IOException e) {
            throw new UnexpectedTechnicalException("Failed to read Response Json body", e);
        }
    }

    public <TEntity> List<TEntity> executeJsonList(OAuthRequest oAuthRequest, Class<TEntity> entityClass) {
        Response response = executeRequest(oAuthRequest);
        return readJsonBodyList(response, entityClass);
    }

    public <TEntity> List<TEntity> readJsonBodyList(Response response, Class<TEntity> entityClass) {
        try {
            String body = response.getBody();
            if (StringUtils.length(body) <= 0) {
                return new ArrayList<>();
            }
            List<TEntity> entities = Json.parseList(body, entityClass);
            return entities;
        } catch (IOException e) {
            throw new UnexpectedTechnicalException("Failed to read Response Json body", e);
        }
    }

    /**
     *
     * @param oAuthRequest
     * @return
     * @throws HttpResponseException if Response HttpStatus not Successful
     */
    public Response executeRequest(OAuthRequest oAuthRequest) throws HttpResponseException {
        Response response = executeRequestRaw(oAuthRequest);
        if (response.isSuccessful()) {
            return response;
        }
        int httpStatus = response.getCode();
        if (httpStatus == HttpStatus.SC_UNAUTHORIZED) {
            // reset token to retry with a new one
            resetAccessToken();
            response = executeRequestRaw(oAuthRequest);
            if (response.isSuccessful()) {
                return response;
            }
        }
        throw new HttpResponseException(oAuthRequest, response);
    }

    private Response executeRequestRaw(OAuthRequest oAuthRequest) {
        signRequest(oAuthService, oAuthRequest, getAccessToken());
        Response response;
        try {
            response = oAuthService.execute(oAuthRequest);
        } catch (Exception e) {
            throw new UnexpectedTechnicalException(e);
        }
        return response;
    }

    public void forceAccessTokenForTest(TToken accessToken) {
        this.accessToken = accessToken;
    }

    private void resetAccessToken() {
        accessToken = null;
    }

    private TToken getAccessToken() {
        if (accessToken == null) {
            try {
                accessToken = accessTokenSupplier.getAccessToken();
                LOG.info("New accessToken successfully retrieved");
            } catch (Exception e) {
                throw new UnexpectedTechnicalException("Failed to retrieve accessToken => check your connection and credentials", e);
            }
        }
        return accessToken;
    }

    protected abstract OAuthRequest signRequest(TOAuthService oAuthService, OAuthRequest oAuthRequest, TToken accessToken);
}
