package co.getbarry.util.http;

import co.getbarry.util.HttpToolbox;
import co.getbarry.util.UnexpectedTechnicalException;
import co.getbarry.util.network.NetworkToolbox;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Verb;
import com.google.common.net.MediaType;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.Map;

public class HttpAuthRequestBuilder {

    private OAuthRequest oAuthRequest;
    // must redefine ObjectMapper since the one of "Json" library doesn't format Date the expected way for slimpay
    private static final ObjectMapper OBJECT_MAPPER;

    static {
        OBJECT_MAPPER = new ObjectMapper();
        OBJECT_MAPPER.registerModule(new JavaTimeModule());
    }

    public HttpAuthRequestBuilder bobyJson(Object boby) {
        if (boby == null) {
            return this;
        }
        oAuthRequest.setPayload(toJsonIfNecessary(boby));
        oAuthRequest.addHeader(HttpToolbox.CONTENT_TYPE_HEADER, MediaType.JSON_UTF_8.toString());
        return this;
    }

    private String toJsonIfNecessary(Object object) {
        if (object instanceof String) {
            return (String) object;
        } else {
            return toJson(object);
        }
    }

    private String toJson(Object obj) {
        try {
            return OBJECT_MAPPER.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new UnexpectedTechnicalException("Write Json in String must not failed", e);
        }
    }

    public HttpAuthRequestBuilder contentType(@NotNull MediaType mediaType) {
        return contentType(mediaType.toString());
    }

    public HttpAuthRequestBuilder contentType(@NotBlank String contentType) {
        oAuthRequest.addHeader(HttpToolbox.CONTENT_TYPE_HEADER, contentType);
        return this;
    }

    private HttpAuthRequestBuilder initMultipartIfNecessary() {
        if (oAuthRequest.getMultipartPayload() == null) {
            oAuthRequest.initMultipartPayload();
        }
        return this;
    }

    public HttpAuthRequestBuilder addHeader(String key, String value) {
        if (StringUtils.isNotBlank(key)) {
            oAuthRequest.addHeader(key, value);
        }
        return this;
    }

    public HttpAuthRequestBuilder addHeaders(Map<String, ?> headers) {
        if (MapUtils.isNotEmpty(headers)) {
            headers.forEach((k, v) -> addHeader(k, v.toString()));
        }
        return this;
    }

    public HttpAuthRequestBuilder addParameter(String key, String value) {
        if (StringUtils.isNotBlank(key)) {
            oAuthRequest.addQuerystringParameter(key, value);
        }
        return this;
    }

    public HttpAuthRequestBuilder addParameters(Map<String, ?> parameters) {
        if (MapUtils.isNotEmpty(parameters)) {
            parameters.forEach((k, v) -> addParameter(k, v.toString()));
        }
        return this;
    }

    public OAuthRequest build(){
        return oAuthRequest;
    }

    HttpAuthRequestBuilder(Verb requestVerb, URI fullUrl) {
        oAuthRequest = new OAuthRequest(requestVerb, fullUrl.toString());
    }

    HttpAuthRequestBuilder(Verb requestVerb, URI baseUrl, URI path) {
        this(requestVerb, buildFullUrl(baseUrl, path));
    }


    public static HttpAuthRequestBuilder newGet(URI baseUrl, URI path) {
        return newFor(Verb.GET, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newGet(URI baseUrl, String path) {
        return newFor(Verb.GET, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newPostOrPut(URI baseUrl, String path, String id) {
        if (StringUtils.isBlank(id)) {
            return newFor(Verb.POST, baseUrl, path);
        } else {
            return newFor(Verb.PUT, baseUrl, path + "/" + id);
        }
    }

    public static HttpAuthRequestBuilder newPut(URI baseUrl, String path) {
        return newFor(Verb.PUT, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newPut(URI baseUrl, URI path) {
        return newFor(Verb.PUT, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newPost(URI baseUrl, String path) {
        return newFor(Verb.POST, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newPost(URI baseUrl, URI path) {
        return newFor(Verb.POST, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newDelete(URI baseUrl, String path) {
        return newFor(Verb.DELETE, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newDelete(URI baseUrl, URI path) {
        return newFor(Verb.DELETE, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newFor(Verb verb, URI baseUrl, URI path) {
        return new HttpAuthRequestBuilder(verb, baseUrl, path);
    }

    public static HttpAuthRequestBuilder newFor(Verb verb, URI baseUrl, String path) {
        return new HttpAuthRequestBuilder(verb, buildFullUrl(baseUrl, path));
    }

    private static URI buildFullUrl(URI baseUrl, URI pathUrl) {
        return buildFullUrl(baseUrl.toString(), pathUrl.toString());
    }

    private static URI buildFullUrl(URI baseUrl, String pathUrl) {
        return buildFullUrl(baseUrl.toString(), pathUrl);
    }

    private static URI buildFullUrl(String baseUrl, String pathUrl) {
        String fullPath = StringUtils.removeEnd(baseUrl, "/") + "/" + StringUtils.removeStart(pathUrl, "/");
        return NetworkToolbox.newUriValid(fullPath);
    }

}
