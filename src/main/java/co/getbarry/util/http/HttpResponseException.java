package co.getbarry.util.http;

import co.getbarry.util.exception.EntityNotFoundException;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.google.api.client.http.HttpStatusCodes;
import org.apache.commons.lang3.StringUtils;

public class HttpResponseException extends RuntimeException {

    private OAuthRequest httpRequest;
    private Response httpResponse;

    protected HttpResponseException(OAuthRequest httpRequest, Response httpResponse) {
        super("HttpCode <" + httpResponse.getCode() + "> (" + httpResponse.getMessage() + ") when calling <" + httpRequest.toString() + "> (" + extractDetailFromBody(httpResponse) + ")");
        this.httpRequest = httpRequest;
        this.httpResponse =  httpResponse;
    }

    protected static String extractDetailFromBody(Response response) {
        try {
            return StringUtils.defaultString(response.getBody(), "");
        } catch (Exception e) {
            return "";
        }
    }

    public Response getHttpResponse() {
        return httpResponse;
    }

    public OAuthRequest getHttpRequest() {
        return httpRequest;
    }

    public int getHttpStatus() {
        return getHttpResponse().getCode();
    }

    public HttpResponseException checkEntityNotFoundException(Class entityClass) throws EntityNotFoundException {
        if (getHttpStatus() == HttpStatusCodes.STATUS_CODE_NOT_FOUND) {
            throw new EntityNotFoundException(entityClass, this);
        }
        return this;
    }

}


