package co.getbarry.util.exception;

public class EntityNotFoundException extends Exception {

    public EntityNotFoundException(Class entityClass, Throwable cause) {
        super("Entity <" + entityClass.getName() + "> not found (" + cause.getMessage() + ")", cause);
    }

    public EntityNotFoundException(Class entityClass, String details) {
        super("Entity <" + entityClass.getName() + "> not found (" + details + ")");
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }
}
