package co.getbarry.api.slimpay;

import co.getbarry.api.slimpay.model.*;
import co.getbarry.model.db.AddressIso;
import co.getbarry.model.db.User;
import co.getbarry.model.db.payment_mean.SepaDirectDebitMandate;
import co.getbarry.util.exception.EntityNotFoundException;
import co.getbarry.util.http.HttpAuthRequestBuilder;
import co.getbarry.util.http.OAuth2Client;
import co.getbarry.util.network.NetworkToolbox;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.apache.http.HttpHost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.money.MonetaryAmount;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.time.ZonedDateTime;

import static co.getbarry.util.BarryUtil.s;

/**
 * https://dev.slimpay.com/hapi/overview/format
 */
@Service
public class SlimpayRawService {

    private static final Logger LOG = LoggerFactory.getLogger(SlimpayRawService.class);
    private String simplayServerUrl;
    private String simplayApiKey;
    private String slimpayApiSecret;

    private OAuth2Client oAuth2Client;
    private HapiResponse serviceLinks;

    public SlimpayRawService(String simplayServerUrl, String simplayApiKey, String slimpayApiSecret) {
        this.simplayServerUrl = simplayServerUrl;
        this.simplayApiKey = simplayApiKey;
        this.slimpayApiSecret = slimpayApiSecret;
        initOAuth2ClientAndServiceLinks();
    }

    public SlimpayRawService initOAuth2ClientAndServiceLinks() {
        OAuth20Service oAuth20Service = new ServiceBuilder(simplayApiKey)
                .apiSecret(slimpayApiSecret)
                .build(new BasicHttpApi(simplayServerUrl));
        oAuth2Client = new OAuth2Client(oAuth20Service, () -> oAuth20Service.getAccessTokenClientCredentialsGrant());
        serviceLinks = getAllServiceLinks();
        return this;
    }

    public OAuth2Client getOAuth2Client() {
        if (oAuth2Client == null) {
            initOAuth2ClientAndServiceLinks();
        }
        return oAuth2Client;
    }

    private HapiResponse getServiceLinks() {
        return serviceLinks;
    }

    private HapiResponse getAllServiceLinks() {
        HapiResponse response = getOAuth2Client().executeJson(
                HttpAuthRequestBuilder.newGet(NetworkToolbox.newUriValid(simplayServerUrl), "").build(),
                HapiResponse.class
        );
        return response;
    }

    private static class BasicHttpApi extends DefaultApi20 {
        private String serverUrl;

        public BasicHttpApi(String serverUrl) {
            this.serverUrl = serverUrl;
        }

        @Override
        public String getAccessTokenEndpoint() {
            return serverUrl + "/oauth/token";
        }
        @Override
        protected String getAuthorizationBaseUrl() {
            return serverUrl + "/oauth/authorize";
        }
    }

    public Payment payByCreditCard(@NotBlank String creditorReference, @NotBlank String subscriberReference, MonetaryAmount amount) {
        PaymentRequest paymentRequest = PaymentRequest.newCreditCardForSubscriber(creditorReference, subscriberReference, amount);
        Payment payment = pay(paymentRequest);
        return payment;
    }

    public Payment payByDirectDebit(@NotBlank String creditorReference, @NotBlank String subscriberReference, @NotNull MonetaryAmount amount) {
        PaymentRequest paymentRequest = PaymentRequest.newCreditCardForSubscriber(creditorReference, subscriberReference, amount);
        Payment payment = pay(paymentRequest);
        return payment;
    }

    public Payment payByDirectDebitWithMandate(@NotBlank String creditorReference, @NotBlank String mandateUniqueReference, @NotNull MonetaryAmount amount) {
        PaymentRequest paymentRequest = PaymentRequest.newDirectDebitForMandate(creditorReference, mandateUniqueReference, amount);
        Payment payment = pay(paymentRequest);
        return payment;
    }

    public Mandate findMandate(@NotBlank String creditorReference, @NotBlank String mandateUniqueReference) throws EntityNotFoundException {
        OAuthRequest findMandateRequest = getServiceLinks().newGetForLink( "get-mandates")
                .addParameter("creditorReference", creditorReference)
                .addParameter("reference", mandateUniqueReference)
                .build();
        Mandate mandate = getOAuth2Client().executeJsonAndCheckNotFound(findMandateRequest, Mandate.class);
        return mandate;
    }

    public Mandate importMandate(@NotBlank String creditorReference, @NotNull @Valid Contact signatorySubscriber, @NotNull ZonedDateTime signatureDate, String mandateUniqueReference) {
        MandateImportRequest mandateImportRequest = MandateImportRequest.builder()
                .reference(mandateUniqueReference)
                .paymentScheme(PaymentScheme.SEPA_DIRECT_DEBIT.getSlimpayValue())
                .creditor(Actor.newReferenced(creditorReference))
                .subscriber(Actor.newReferenced(signatorySubscriber.getReferenceId()))
                .dateSigned(signatureDate)
                .signatory(signatorySubscriber)
                .build();
        Mandate mandate = importMandate(mandateImportRequest);
        return mandate;
    }

    private Payment pay(PaymentRequest paymentRequest) {
        OAuthRequest authRequest = getServiceLinks().newPostForLink("create-payins")
                .bobyJson(paymentRequest)
                .build();
        Payment payment = getOAuth2Client().executeJson(authRequest, Payment.class);
        return payment;
    }

    private Mandate importMandate(MandateImportRequest mandateImportRequest) {
        OAuthRequest createMandateRequest = getServiceLinks().newPostForLink( "create-mandates")
                .bobyJson(mandateImportRequest)
                .build();
        Mandate mandateBack = getOAuth2Client().executeJson(createMandateRequest, Mandate.class);
        LOG.info(s("Mandate with reference %s successfully imported", mandateBack.getReference()));
        return mandateBack;
    }

}
