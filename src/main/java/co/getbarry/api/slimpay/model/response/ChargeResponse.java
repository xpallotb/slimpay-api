package co.getbarry.api.slimpay.model.response;

/**
 * Represents the response when customer is charged.
 *
 * @author Alpar Nagy
 */
public class ChargeResponse {

    private String id;

    private String clientSecret;

    private PaymentIntentStatus status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public PaymentIntentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentIntentStatus status) {
        this.status = status;
    }
}
