package co.getbarry.api.slimpay.model;

public class TokenPaymentMean implements PaymentMean {

	private String token;

	@Override
	public String getId() {
		return token;
	}

	@Override
	public void setId(String id) {
		this.token = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
