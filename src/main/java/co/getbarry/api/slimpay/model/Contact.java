package co.getbarry.api.slimpay.model;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
    private String referenceId;
    private String honorificPrefix;
    private String givenName;
    private String familyName;
    private String email;
    private String telephone;
    private Address billingAddress;
    private BankAccount bankAccount;
}
