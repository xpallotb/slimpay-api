package co.getbarry.api.slimpay.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import javax.money.MonetaryAmount;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Getter
public class PaymentRequest {
    private Actor creditor;
    private Actor subscriber;
    private Actor mandate;
    private String reference;
    private Double amount;
    private String currency;
    private String scheme;
    private String label;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime executionDate;

    private PaymentRequest scheme(PaymentScheme paymentScheme) {
        this.scheme = paymentScheme.getSlimpayValue();
        return this;
    }

    public static PaymentRequest newDirectDebitForSubscriber(String creditorRefence, String subscriberReference, MonetaryAmount amount) {
        PaymentRequest paymentRequest = newRequestDefault(creditorRefence, subscriberReference, null, amount).scheme(PaymentScheme.SEPA_DIRECT_DEBIT);

        return paymentRequest;
    }

    public static PaymentRequest newDirectDebitForMandate(String creditorRefence, String mandateUniqueReference, MonetaryAmount amount) {
        PaymentRequest paymentRequest = newRequestDefault(creditorRefence, null, mandateUniqueReference, amount).scheme(PaymentScheme.SEPA_DIRECT_DEBIT);

        return paymentRequest;
    }

    public static PaymentRequest newCreditCardForSubscriber(String creditorRefence, String subscriberReference, MonetaryAmount amount) {
        PaymentRequest paymentRequest = newRequestDefault(creditorRefence, subscriberReference, null, amount).scheme(PaymentScheme.CREDIT_CARD);
        return paymentRequest;
    }

    private static PaymentRequest newRequestDefault(String creditorReference, String subscriberReference, String mandateUniqueReference, MonetaryAmount amount) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.creditor = Actor.newReferenced(creditorReference);
        if (StringUtils.isNotBlank(subscriberReference)) {
            paymentRequest.subscriber = Actor.newReferenced(subscriberReference);
        }
        if (StringUtils.isNotBlank(mandateUniqueReference)) {
            paymentRequest.mandate = Actor.newReferenced(mandateUniqueReference);
        }
        paymentRequest.amount = amount.getNumber().doubleValue();
        paymentRequest.currency = amount.getCurrency().getCurrencyCode();
        return paymentRequest;
    }

}
