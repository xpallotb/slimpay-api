package co.getbarry.api.slimpay.model;

public enum  PaymentDirection {
    IN, OUT
}
