package co.getbarry.api.slimpay.model.response;

/**
 * @author Alpar Nagy
 */
public enum PaymentIntentStatus {
    REQUIRES_PAYMENT_METHOD,
    REQUIRES_CONFIRMATION,
    REQUIRES_ACTION,
    PROCESSING,
    CANCELED,
    SUCCEEDED;
}
