package co.getbarry.api.slimpay.model.response;

import co.getbarry.api.slimpay.model.request.PaymentCustomer;

import java.math.BigDecimal;

/**
 * Represents the response when customer is refunded.
 *
 * @author Alpar Nagy
 */
public class RefundResponse {

    private String id;

    private PaymentCustomer customer;

    private BigDecimal amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PaymentCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(PaymentCustomer customer) {
        this.customer = customer;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
