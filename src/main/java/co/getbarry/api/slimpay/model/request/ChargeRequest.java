package co.getbarry.api.slimpay.model.request;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Represents the request for charging the customer.
 *
 * @author Alpar Nagy
 */
public class ChargeRequest {

    /**
     * Optional. Payment mean id. If not set default payment mean is used.
     */
    private String paymentMeanId;

    /**
     * Optional. Charge description.
     */
    private String description;

    private Currency currency = Currency.getInstance("DKK");

    /**
     * A positive integer representing how much to charge. Minimum amount is 2.5 DKK.
     */
    private BigDecimal amount;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPaymentMeanId() {
        return paymentMeanId;
    }

    public void setPaymentMeanId(String paymentMeanId) {
        this.paymentMeanId = paymentMeanId;
    }
}
