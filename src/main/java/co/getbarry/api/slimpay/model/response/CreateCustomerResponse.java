package co.getbarry.api.slimpay.model.response;

/**
 * Represents the response a new customer is created.
 *
 * @author Alpar Nagy
 */
public class CreateCustomerResponse {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
