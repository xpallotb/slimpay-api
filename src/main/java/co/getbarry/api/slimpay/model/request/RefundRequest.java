package co.getbarry.api.slimpay.model.request;

import java.math.BigDecimal;

/**
 * Represents the request for creating a new customer.
 *
 * @author Alpar Nagy
 */
public class RefundRequest {

    /**
     * The payment intent id or charge id received when the customer was charged.
     */
    private String chargeId;

    /**
     * Optional. A positive integer representing how much of this charge to refund. Can refund only up to the remaining,
     * unrefunded amount of the charge. Value can be null in case of full refund.
     */
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }
}
