package co.getbarry.api.slimpay.model;

import co.getbarry.util.http.HttpAuthRequestBuilder;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class HapiResponse {

    private static final String LINKS_NAMESPACE = "https://api.slimpay.net/alps#";

    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private Map<String, URI> links = new HashMap<>();

    private Map<String, Object> otherFields = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getOtherFields() {
        return otherFields;
    }

    @JsonAnySetter
    public void setOtherField(String name, Object value) {
        otherFields.put(name, value);
    }

    public URI getLinkUrl(String linkBaseKey) {
        String linkFullKey = LINKS_NAMESPACE + linkBaseKey;
        if (!links.containsKey(LINKS_NAMESPACE + linkBaseKey)) {
            throw new IllegalArgumentException("Unknown link with fullKey <" + linkFullKey + ">");
        }
        return links.get(linkFullKey);
    }


    public HttpAuthRequestBuilder newGetForLink(String linkBaseKey) {
        return HttpAuthRequestBuilder.newGet(getLinkUrl(linkBaseKey), "");
    }

    public HttpAuthRequestBuilder newPostForLink(String linkBaseKey) {
        return HttpAuthRequestBuilder.newPost(getLinkUrl(linkBaseKey), "");
    }

}
