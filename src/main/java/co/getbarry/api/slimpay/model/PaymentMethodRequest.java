package co.getbarry.api.slimpay.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PaymentMethodRequest {

    private Boolean started;
    private String locale;
    private String paymentScheme;
    private Actor creditor;
    private Actor subscriber;
    private List<RequestItem> items = new ArrayList<>();

    @Getter
    public static class RequestItem {
        private String type;
        private String action;
        private Mandate mandate;
        private SignatureApproval signatureApproval;

        @Getter
        public static class Mandate {
            private Contact signatory;
            public static Mandate newForSubscriber(Contact subscriber) {
                Mandate mandate = new Mandate();
                mandate.signatory = subscriber;
                return mandate;
            }
        }

        public RequestItem checkboxSignature() {
            signatureApproval = SignatureApproval.newCheckboxMethod();
            return this;
        }

        static RequestItem newMandate(Contact subscriber) {
            RequestItem requestItem = new RequestItem();
            requestItem.type = "signMandate";
            requestItem.action = "sign";
            requestItem.mandate = Mandate.newForSubscriber(subscriber);
            return requestItem;

        }

        static RequestItem newCreditCard() {
            RequestItem requestItem = new RequestItem();
            requestItem.type = "cardAlias";
            return requestItem;
        }

    }

    private PaymentMethodRequest itemsAdd(RequestItem requestItem) {
        items.add(requestItem);
        return this;
    }

    private PaymentMethodRequest paymentScheme(PaymentScheme paymentScheme) {
        this.paymentScheme = paymentScheme.getSlimpayValue();
        return this;
    }

    public static PaymentMethodRequest newDirectDebit(String creditorRefence, Contact subscriber) {
        PaymentMethodRequest paymentMethodRequest = newRequestDefault(creditorRefence, subscriber).paymentScheme(PaymentScheme.SEPA_DIRECT_DEBIT);
        paymentMethodRequest.itemsAdd(RequestItem.newMandate(subscriber).checkboxSignature());
        return paymentMethodRequest;
    }

    public static PaymentMethodRequest newCreditCard(String creditorRefence, Contact subscriber) {
        PaymentMethodRequest paymentMethodRequest = newRequestDefault(creditorRefence, subscriber).paymentScheme(PaymentScheme.CREDIT_CARD);
        paymentMethodRequest.itemsAdd(RequestItem.newCreditCard());
        return paymentMethodRequest;
    }

    private static PaymentMethodRequest newRequestDefault(String creditorRefence, Contact subscriber) {
        PaymentMethodRequest paymentMethodRequest = new PaymentMethodRequest();
        paymentMethodRequest.started = true;
        paymentMethodRequest.creditor = Actor.newReferenced(creditorRefence);
        paymentMethodRequest.subscriber = Actor.newReferenced(subscriber.getReferenceId());
        return paymentMethodRequest;
    }

}
