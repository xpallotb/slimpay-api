package co.getbarry.api.slimpay.model;

import co.getbarry.util.network.NetworkToolbox;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Streams;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class LinksDeserializer extends JsonDeserializer<Map<String, URI>> {
    @Override
    public Map<String, URI> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode jsonNodes = jsonParser.getCodec().readTree(jsonParser);
        Map<String, URI> links = new HashMap<>();
        Streams.stream(jsonNodes.fieldNames()).forEach(linkName -> {
            String linkString = jsonNodes.path(linkName).path("href").asText();
            URI linkUrl = NetworkToolbox.newUriValid(StringUtils.substringBefore(linkString, "{"));
            links.put(linkName, linkUrl);
        } );
        return links;
    }
}
