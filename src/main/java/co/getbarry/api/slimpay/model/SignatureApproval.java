package co.getbarry.api.slimpay.model;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignatureApproval {

    @Setter
    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Method {
        String type;
    }

    private Method method;

    public static SignatureApproval newCheckboxMethod() {
        return builder().method(Method.builder().type("checkbox").build()).build();
    }

}
