package co.getbarry.api.slimpay.model;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BankAccount {

    private String iban;

    public static BankAccount newIban(String iban) {
        return BankAccount.builder().iban(iban).build();
    }

}
