package co.getbarry.api.slimpay.model.request;

/**
 * Represents the request for creating a new customer.
 *
 * @author Alpar Nagy
 */
public class PaymentCustomer {

    private String id;

    private String description;

    private String email;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
