package co.getbarry.api.slimpay.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.ZonedDateTime;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MandateImportRequest {

    private String reference;
    private Actor creditor;
    private Actor subscriber;
    private String paymentScheme;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime dateSigned;
    private Contact signatory;

}
