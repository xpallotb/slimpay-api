package co.getbarry.api.slimpay.model;

public enum PaymentScheme {
    SEPA_DIRECT_DEBIT("SEPA.DIRECT_DEBIT.CORE"),
    CREDIT_CARD("CARD");

    private String slimpayValue;

    PaymentScheme(String slimpayValue) {
        this.slimpayValue = slimpayValue;
    }

    public String getSlimpayValue() {
        return slimpayValue;
    }
}
