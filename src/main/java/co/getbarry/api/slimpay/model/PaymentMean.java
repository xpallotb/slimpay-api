package co.getbarry.api.slimpay.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value = CreditCard.class),
	@JsonSubTypes.Type(value = TokenPaymentMean.class)
})
public interface PaymentMean {
    String getId();

	void setId(String id);
}
