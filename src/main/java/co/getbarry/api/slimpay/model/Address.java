package co.getbarry.api.slimpay.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;
import org.eastinghouse.model.simplex.type.Country;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Address {
    @JsonProperty("street1")
    private String street;
    private String city;
    private String postalCode;
    private Country country;
}
