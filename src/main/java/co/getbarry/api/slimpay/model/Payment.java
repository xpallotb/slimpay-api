package co.getbarry.api.slimpay.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.time.ZonedDateTime;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Payment extends HapiResponse {
    private String id;
    private String reference;
    private PaymentScheme paymentScheme;
    private PaymentDirection direction;
    private Double amount;
    private String currency;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private ZonedDateTime executionDate;

}
