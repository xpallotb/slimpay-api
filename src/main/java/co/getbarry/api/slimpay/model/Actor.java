package co.getbarry.api.slimpay.model;

import lombok.Getter;

import javax.validation.constraints.NotBlank;

@Getter
public class Actor {
    @NotBlank
    private String reference;

    public Actor(String reference) {
        this.reference = reference;
    }

    public static Actor newReferenced(String reference) {
        return new Actor(reference);
    }
}