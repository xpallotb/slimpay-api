package co.getbarry.api.slimpay;

import co.getbarry.api.slimpay.model.*;
import co.getbarry.model.db.AddressIso;
import co.getbarry.model.db.User;
import co.getbarry.model.db.payment_mean.SepaDirectDebitMandate;
import co.getbarry.util.Json;
import co.getbarry.util.exception.EntityNotFoundException;
import co.getbarry.util.http.HttpAuthRequestBuilder;
import co.getbarry.util.http.OAuth2Client;
import co.getbarry.util.network.NetworkToolbox;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.money.MonetaryAmount;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.time.ZonedDateTime;

/**
 * https://dev.slimpay.com/hapi/overview/format
 */
@Service
public class SlimpayService {

    @Autowired
    private SlimpayRawService slimpayRawService;

    public SlimpayService(SlimpayRawService slimpayRawService) {
        this.slimpayRawService = slimpayRawService;
    }

    public String payByDirectDebit(@NotNull @Valid SepaDirectDebitMandate sepaDirectDebitMandate, @NotNull MonetaryAmount amount) {
        Payment payment = slimpayRawService.payByDirectDebitWithMandate(
                sepaDirectDebitMandate.getCreditorIdentifier(),
                sepaDirectDebitMandate.getMandateUniqueReference(),
                amount
        );
        return payment.getReference();
    }

    public SepaDirectDebitMandate importMandate(@NotNull @Valid SepaDirectDebitMandate sepaDirectDebitMandate) {
        if (!sepaDirectDebitMandate.isSigned()) {
            throw new IllegalArgumentException("SepaDirectDebitMandate must be signed before using this function");
        }
        User debtorUser = sepaDirectDebitMandate.getDebtorUser();
        Contact signatory = toContact(debtorUser, sepaDirectDebitMandate.getDebtorAddress(), sepaDirectDebitMandate.getDebtorIban());
        System.out.println(Json.stringifyPretty(signatory));
        slimpayRawService.importMandate(
                sepaDirectDebitMandate.getCreditorIdentifier(),
                signatory,
                sepaDirectDebitMandate.getSignatureDate(),
                sepaDirectDebitMandate.getMandateUniqueReference()
        );
        return sepaDirectDebitMandate;
    }

    private Contact toContact(User user, AddressIso billingAddress, String iban) {
        return Contact.builder()
                .referenceId(user.getCustomerId())
                .givenName(user.getFirstName())
                .familyName(user.getLastName())
                .billingAddress(billingAddress != null ? toAddress(billingAddress) : null)
                .email(user.getEmail())
                .telephone(user.getPhone())
                .bankAccount(StringUtils.isNotBlank(iban) ? BankAccount.newIban(iban) : null)
                .build();
    }

    private Address toAddress(AddressIso addressIso) {
        return Address.builder()
                .street(addressIso.getStreet())
                .postalCode(addressIso.getPostalCode())
                .city(addressIso.getCity())
                .country(addressIso.getCountry())
                .build();
    }

}
