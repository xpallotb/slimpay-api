package co.getbarry.api.slimpay.exception;

@SuppressWarnings("serial")
public class PaymentException extends Exception {
    public PaymentException() {
		super();
	}

	public PaymentException(String message, Throwable cause) {
        super(message, cause);
    }

    public PaymentException(Throwable cause) {
        super(cause);
    }

    public PaymentException(String message) {
        super(message);
    }
}
