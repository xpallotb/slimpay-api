package co.getbarry.api.slimpay;

import co.getbarry.api.slimpay.exception.PaymentException;
import co.getbarry.api.slimpay.model.CreditCard;
import co.getbarry.api.slimpay.model.PaymentMean;
import co.getbarry.api.slimpay.model.request.ChargeRequest;
import co.getbarry.api.slimpay.model.request.PaymentCustomer;
import co.getbarry.api.slimpay.model.request.RefundRequest;
import co.getbarry.api.slimpay.model.response.ChargeResponse;
import co.getbarry.api.slimpay.model.response.RefundResponse;

import java.util.List;


/**
 * Common interface for the supported payment providers.
 *
 * @author Alpar Nagy
 */
public interface PaymentProvider {

    List<String> listAllCustomerIds() throws PaymentException;

    PaymentCustomer getCustomer(String customerId) throws PaymentException;

    List<CreditCard> listPaymentMeans(String customerId) throws PaymentException;

    void saveCustomer(PaymentCustomer paymentCustomer) throws PaymentException;

    boolean deleteCustomer(String customerId) throws PaymentException;

    boolean detachPaymentMethod(String customerId, String paymentMeanId) throws PaymentException;

    RefundResponse refund(RefundRequest request) throws PaymentException;

    String createSetupIntent() throws PaymentException;

    CreditCard attachPaymentMethod(String customerId, PaymentMean paymentMean) throws PaymentException;

    ChargeResponse createPaymentIntent(String customerId, ChargeRequest request) throws PaymentException;

    ChargeResponse getPaymentIntent(String customerId, String paymentIntentId) throws PaymentException;

    CreditCard getPaymentMethodForPaymentIntent(String customerId, String paymentIntentId) throws PaymentException;
}
